import React from "react";
import styled from "styled-components";
import { StyledCarouselPhoto as CarouselPhoto } from "./CarouselPhoto";

const AnimationPhoto = ({ className }) => {
  return (
    <div className={className}>
      <div className="room-photo">
        <CarouselPhoto />;
      </div>
    </div>
  );
};

export const StyledAnimationPhoto = styled(AnimationPhoto)`
  .room-photo {
    height: 1000px;
    width: 100%;
  }
`;
