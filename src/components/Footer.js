import React from "react";
import styled from "styled-components";

const Footer = ({ className }) => {
  return (
    <div className={className}>
      <div className="footer">Footer</div>
    </div>
  );
};

export const StyledFooter = styled(Footer)`
  .footer {
    height: 400px;
    background-color: white;
  }
`;
