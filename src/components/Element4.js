import React from "react";
import styled from "styled-components";

const Element4 = ({ className }) => {
  return (
    <div className={className}>
      <div className="element3-info">Umów się</div>
    </div>
  );
};

export const StyledElement4 = styled(Element4)`
  .element3-info {
    color: white;
    height: 300px;
    background-color: black;
  }
`;
