import React from "react";
import { Carousel, Image } from "react-bootstrap";
import room from "../photos/room.jpg";
import styled from "styled-components";

export const CarouselPhoto = ({ className }) => {
  return (
    <div className={className}>
      <Carousel>
        <Carousel.Item>
          <img className="photo" src={room} alt="First slide" />
          <Carousel.Caption>
            <h3>First slide label</h3>
            <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
          </Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item>
          <img className="photo" src={room} alt="Third slide" />

          <Carousel.Caption>
            <h3>Second slide label</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
          </Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item>
          <img className="photo" src={room} alt="Third slide" />

          <Carousel.Caption>
            <h3>Third slide label</h3>
            <p>
              Praesent commodo cursus magna, vel scelerisque nisl consectetur.
            </p>
          </Carousel.Caption>
        </Carousel.Item>
      </Carousel>
    </div>
  );
};

export const StyledCarouselPhoto = styled(CarouselPhoto)`
  .photo {
    width: 100%;
    height: 800px;
  }
`;
