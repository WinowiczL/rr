import React from "react";
import styled from "styled-components";
import kontakt from "../icons/kontakt.png";
import konsultacja from "../icons/konsultacja.png";
import koncepcja from "../icons/koncepcja.png";
import remont from "../icons/remont.png";
import hendshake from "../icons/hendshake.png";

const Element2 = ({ className }) => {
  return (
    <div className={className}>
      <div className="container">
        <div className="title">
          <span>{title}</span>
        </div>
        <div className="row">
          <StyledContent
            className="col custom-col"
            text={text1}
            contentTitle={contentTitle1}
          >
            <img src={kontakt} className="icon" />
          </StyledContent>
          <StyledContent
            className="col custom-col"
            text={text2}
            contentTitle={contentTitle2}
          >
            <img src={konsultacja} className="icon" />
          </StyledContent>
          <StyledContent
            className="col custom-col"
            text={text3}
            contentTitle={contentTitle3}
          >
            <img src={koncepcja} className="icon" />
          </StyledContent>
          <StyledContent
            className="col custom-col"
            text={text4}
            contentTitle={contentTitle4}
          >
            <img src={remont} className="icon" />
          </StyledContent>{" "}
          <StyledContent
            className="col custom-col"
            text={text5}
            contentTitle={contentTitle5}
          >
            <img src={hendshake} className="icon" />
          </StyledContent>
        </div>
      </div>
    </div>
  );
};

const Content = ({ className, text, contentTitle, children }) => {
  return (
    <div className={className}>
      <div className="content content-a">
        {children}
        <span className="content-title">{contentTitle}</span>
        <br />
        <span className="text">{text}</span>
      </div>
    </div>
  );
};

const StyledContent = styled(Content)`
  .content {
    text-align: center;
    padding: 16px;
    display: grid;
    justify-items: center;
  }

  .content-title {
    font-weight: 600;
  }
  .text {
    font-size: 16px;
  }
  .icon {
    height: 64px;
    width: 64px;
  }
`;

export const StyledElement2 = styled(Element2)`
  margin-top: 40px;
  .custom-col {
    padding: 0;
  }
  .title {
    font-size: 20px;
    font-weight: 800;
    text-align: center;
    margin-bottom: 20px;
  }
  .element3-info {
    height: 800px;
    background-color: white;
  }
`;

const title = "WSPÓŁPRACA KROK PO KROKU";
const contentTitle1 = "Pierwszy kontak";
const text1 =
  "To już pewne, nadszedł czas na zmiany. Możesz do nas zadzwonić, napisać E-mail lub skontaktować się przez formularz na stronie. Wybierz sposób, który jest dla Ciebie najbardziej odpowiedni.";

const contentTitle2 = "Konsultacja";
const text2 =
  "To moment, gdy możemy zapoznać się z wnętrzem, porozmawiać o Twoich oczekiwaniach i podzielić się naszą fachową opinią. Na tym etapie można spodziewać się również wyceny usługi.";

const contentTitle3 = "Przedstawienie koncepcji";
const text3 =
  "Na podstawie wieloletniego doświadczenia w branży oraz w odpowiedzi na Twoje spersonalizowane potrzeb, prezentujemy koncepcję zmiany, która będzie w pełni dostosowana do stylu życia Twojej rodziny, nowoczesnych trendów i sprawdzonych rozwiązań.";

const contentTitle4 = "Remont, czyli rewolucja wchodzi w życie.";
const text4 =
  "To czas, gdy realizujemy wspólnie ustalone cele, a Ty możesz odprężyć się i z niecierpliwością oczekiwać efektów.";

const contentTitle5 = " Prezentacja efektów";
const text5 =
  "Przedstawiamy owoc naszej współpracy, połączenie Twoich marzeń i naszego profesjonalizmu. ";
