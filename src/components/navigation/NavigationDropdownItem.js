import React from "react";
import styled from "styled-components";
import { Link } from "react-router-dom";

export const NavigationDropdownItem = ({ name, className }) => {
  return (
    <div className={className}>
      <div className="dropdown">
        <div className="nav-item">Realizacje</div>
        <div className="dropdown-content">
          <Link className="dropdown-item" to="/realizacje/kuchnia">
            Kuchnie
          </Link>
          <Link className="dropdown-item" to="/realizacje/salon">
            Salony
          </Link>
          <Link className="dropdown-item" to="/realizacje/lazienka">
            Łazienki
          </Link>
          <Link className="dropdown-item" to="/realizacje/jadalnia">
            Jadalnie
          </Link>
          <Link className="dropdown-item" to="/realizacje/sypialnia">
            Sypialnie
          </Link>
        </div>
      </div>
    </div>
  );
};

export const StyledNavigationDropdownItem = styled(NavigationDropdownItem)`
  .nav-item {
    :hover {
      cursor: pointer;
    }
  }

  .dropdown {
    position: relative;
    display: inline-block;
  }

  .dropdown-item {
    :hover {
      cursor: pointer;
    }
  }

  .dropdown-content {
    display: none;
    position: absolute;
    background-color: #f1f1f1;
    min-width: 160px;
    box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
    z-index: 1;
  }

  .dropdown-item {
    color: black;
    padding: 12px 16px;
    text-decoration: none;
    display: block;
  }

  /* Change color of dropdown links on hover */
  .dropdown-item:hover {
    background-color: black;
    color: white;
  }

  /* Show the dropdown menu on hover */
  .dropdown:hover .dropdown-content {
    display: block;
  }

  /* Change the background color of the dropdown button when the dropdown content is shown */
  .dropdown:hover .dropbtn {
    background-color: #3e8e41;
  }
`;
