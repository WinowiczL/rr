import React from "react";
import styled from "styled-components";
import { StyledNavigationDropdownItem as NavigationDropdownItem } from "./NavigationDropdownItem";
import { NavLink } from "react-router-dom";
const Navigation = ({ className, handleClick, selectedSite }) => {
  return (
    <div className={className}>
      <div className="container navi">
        <nav className="navbar navbar-light custom-nav">
          <NavLink
            exact
            to="/"
            className="link"
            activeClassName="selected link"
          >
            <span>Strona główna</span>
          </NavLink>
          <NavLink
            to="/realizacje"
            className="link"
            activeClassName="selected link"
          >
            <NavigationDropdownItem name="Realizacje" />
          </NavLink>
          <NavLink
            to="/oferta"
            className="link"
            activeClassName="selected link"
          >
            <span>Oferta</span>
          </NavLink>
          <NavLink to="/o-nas" className="link" activeClassName="selected link">
            <span>O nas</span>
          </NavLink>
          <NavLink
            to="/kontakt"
            className="link"
            activeClassName="selected link"
          >
            <span>Kontakt</span>
          </NavLink>
        </nav>
      </div>
    </div>
  );
};

export const StyledNavigation = styled(Navigation)`
  border-bottom: 1px solid gray;
  height: 50px;

  .selected {
    border-bottom: 1px solid black;
  }

  .link {
    color: black;
    text-decoration: none;

    :hover {
      border-bottom: 1px solid black;
      cursor: pointer;
    }
  }

  .nav-item {
    border-bottom: ${props => props.selectedSite && `1px solid black`};
  }

  .navi {
    padding: 0;
  }

  .custom-nav {
    font-size: 20px;
    height: 50px;
    padding: 0;
  }
`;
