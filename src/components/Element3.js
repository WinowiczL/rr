import React from "react";
import styled from "styled-components";

const Element3 = ({ className }) => {
  return (
    <div className={className}>
      <div className="container">
        <div className="element2-info">Element3</div>
      </div>
    </div>
  );
};

export const StyledElement3 = styled(Element3)`
  .element2-info {
    height: 500px;
    background-color: gray;
  }
`;
