import React from "react";
import styled from "styled-components";
import facebook from "../icons/facebook.png";

const Header = ({ className }) => {
  return (
    <div className={className}>
      <div className="container">
        <div className="row justify-content-between">
          <div className="logo">Remontowe rewolucje (LOGO)</div>
          <div>
            <a href="https://www.facebook.com/remontowerewolucje/">
              <img src={facebook} alt="facebook" className="icon" />
            </a>
          </div>
        </div>
      </div>
    </div>
  );
};

export const StyledHeader = styled(Header)`
  color: white;
  background-color: black;
  height: 80px;
  display: flex;
  align-items: center;

  .logo {
    align-self: center;
  }
  .icon {
    margin-left: 20px;
    width: 40px;
    height: 40px;

    :hover {
      cursor: pointer;
    }
  }
`;
