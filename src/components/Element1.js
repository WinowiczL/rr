import React from "react";
import styled from "styled-components";

const Element1 = ({ className }) => {
  return (
    <div className={className}>
      <div className="container">
        <div className="short-info">Short info</div>
      </div>
    </div>
  );
};

export const StyledElement1 = styled(Element1)`
  .short-info {
    height: 300px;
    background-color: white;
  }
`;
