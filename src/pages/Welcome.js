import React from "react";
import { StyledAnimationPhoto as AnimationPhoto } from "../components/AnimationPhoto";
import { StyledElement1 as Element1 } from "../components/Element1";
import { StyledElement2 as Element2 } from "../components/Element2";
import { StyledElement3 as Element3 } from "../components/Element3";
import { StyledElement4 as Element4 } from "../components/Element4";
export const Welcome = () => {
  return (
    <>
      <AnimationPhoto />
      <Element2 />
      <Element3 />
      <Element1 />
      <Element4 />
    </>
  );
};
