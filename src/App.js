import React from "react";
import "./App.css";
import { Routing } from "./routing";

export class App extends React.Component {
  render() {
    return <Routing />;
  }
}
export default App;
