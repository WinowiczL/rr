import React from "react";
import { Welcome } from "../pages/Welcome";
import { StyledHeader as Header } from "../components/Header";
import { StyledFooter as Footer } from "../components/Footer";
import { StyledNavigation as Navigation } from "../components/navigation/Navigation";
import { StyledAboutUs as AboutUs } from "../pages/AboutUs";
import { StyledRealizations as Realizations } from "../pages/realizations/Realizations";
import { StyledOffer as Offer } from "../pages/Offer";
import { StyledContact as Contact } from "../pages/Contact";
import { BrowserRouter, Route } from "react-router-dom";
import { Bathroom } from "../pages/realizations/Bathroom";
import { Bedroom } from "../pages/realizations/Bedroom";
import { Diningroom } from "../pages/realizations/Diningroom";
import { Kitchen } from "../pages/realizations/Kitchen";
import { Salon } from "../pages/realizations/Salon";

export const Routing = ({ className }) => {
  return (
    <div className={className}>
      <BrowserRouter>
        <Header />
        <Navigation />
        <Route path="/" exact component={Welcome} />
        <Route path="/kontakt" component={Contact} />
        <Route path="/oferta" component={Offer} />
        <Route path="/realizacje" exact component={Realizations} />
        <Route path="/o-nas" component={AboutUs} />
        <Route path="/realizacje/lazienka" component={Bathroom} />
        <Route path="/realizacje/jadalnia" component={Diningroom} />
        <Route path="/realizacje/kuchnia" component={Kitchen} />
        <Route path="/realizacje/sypialnia" component={Bedroom} />
        <Route path="/realizacje/salon" component={Salon} />
        <Footer />
      </BrowserRouter>
    </div>
  );
};
